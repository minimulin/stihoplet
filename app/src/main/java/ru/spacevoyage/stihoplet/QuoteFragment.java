package ru.spacevoyage.stihoplet;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuoteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuoteFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class QuoteFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private String[] colors = {"#9C5C5B","#2B3E50","#5F586B","#00819E","#9B4C71","#769D93","#683136"
            ,"#28795D","#828B3C","#457698","#764572"};

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    // TODO: Rename and change types and number of parameters
    public static QuoteFragment newInstance() {
        QuoteFragment fragment = new QuoteFragment();

        return fragment;
    }
    public QuoteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        new ParseSite().execute("http://autopoet.yandex.ru");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quote, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public class HtmlHelper {
        TagNode rootNode;

        //Конструктор
        public HtmlHelper(URL htmlPage) throws IOException
        {
            //Создаём объект HtmlCleaner
            HtmlCleaner cleaner = new HtmlCleaner();
            //Загружаем html код сайта
            rootNode = cleaner.clean(htmlPage);
        }

        List<TagNode> getLinksByClass(String cssClassname)
        {
            List<TagNode> linkList = new ArrayList<TagNode>();

            //Выбираем все ссылки
            TagNode linkElements[] = rootNode.getElementsByName("div", true);
            for (int i = 0; linkElements != null && i < linkElements.length; i++)
            {
                //получаем атрибут по имени
                String classType = linkElements[i].getAttributeByName("class");
                //если атрибут есть и он эквивалентен искомому, то добавляем в список
                if (classType != null && classType.equals(cssClassname))
                {
                    linkList.add(linkElements[i]);
                }
            }

            return linkList;
        }
    }

    private class ParseSite extends AsyncTask<String, Void, String> {
        //Фоновая операция
        protected String doInBackground(String... arg) {
            String output = new String();
            try
            {
                HtmlHelper hh = new HtmlHelper(new URL(arg[0]));
                List<TagNode> links = hh.getLinksByClass("poem__text");

                for (Iterator<TagNode> iterator = links.iterator(); iterator.hasNext();) {
                    TagNode[] pElements = ((TagNode) iterator.next()).getElementsByName("p",true);
                    TagNode pElement = pElements[0];

                    for (int i = 0; i < pElement.getAllChildren().size(); i++) {
                        try {
                            ContentNode curContNode = (ContentNode)pElement.getAllChildren().get(i);
                            String oneLine = curContNode.getContent().trim();
                            oneLine = oneLine.substring(0, 1).toUpperCase() + oneLine.substring(1).toLowerCase();

                            output = output + oneLine + "\n";
                        } catch (Exception e) {

                        }
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return output;
        }

        protected void onPostExecute(String output) {
            TextView textView = (TextView) getActivity().findViewById(R.id.main_text);
            textView.setText(output);



            FrameLayout relativeLayout = (FrameLayout) getActivity().findViewById(R.id.main_container);
            Random ran = new Random();
            Integer cnt = ran.nextInt(colors.length);
            relativeLayout.setBackgroundColor(Color.parseColor(colors[cnt]));
        }
    }

}
