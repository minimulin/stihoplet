package ru.spacevoyage.stihoplet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Random;


public class MainActivity extends ActionBarActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Handler mainHandler;
    private String[] colors = {"#9C5C5B","#2B3E50","#5F586B","#00819E","#9B4C71","#769D93","#683136"
            ,"#28795D","#828B3C","#457698","#764572"};
    SwipeRefreshLayout swipeRefreshLayout;
    Animation fadeIn, fadeOut;
    ShareActionProvider mActionProvider;
    String currentPoem;

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentPoem = "";

        fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(200);

        fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setDuration(500);

        checkOnlineAndRun();
    }

    public void checkOnlineAndRun() {
        if (!isOnline()) {
            buildDialog(this).show();
        } else {
            swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.main_container);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_light,
                    android.R.color.white, android.R.color.holo_blue_light,
                    android.R.color.white);

            new ParseSite(this).execute("http://autopoet.yandex.ru");
        }
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("Ой!");
        builder.setMessage("Отсутствует доступ к сети Интернет");

        builder.setPositiveButton("Повторить", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                checkOnlineAndRun();
            }
        });

        return builder;
    }

    @Override
    public void onRefresh() {
        AnimationSet animation = new AnimationSet(false); //change to false
        animation.addAnimation(fadeOut);

        TextView textView = (TextView) findViewById(R.id.main_text);

        textView.startAnimation(animation);

        new ParseSite(this).execute("http://autopoet.yandex.ru");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 5000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu
        getMenuInflater().inflate(R.menu.main, menu);

        // Find the share item
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);

        // Need to use MenuItemCompat to retrieve the Action Provider
        mActionProvider = (ShareActionProvider)
                MenuItemCompat.getActionProvider(shareItem);

        return super.onCreateOptionsMenu(menu);
    }

    private void setShareIntent(Intent shareIntent) {
        if (mActionProvider != null) {
            mActionProvider.setShareIntent(shareIntent);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private class ParseSite extends AsyncTask<String, Void, String[]> {
        private Activity parentActivity;

        public ParseSite(Activity activity) {
            parentActivity = activity;
        }
        //Фоновая операция
        protected String[] doInBackground(String... arg) {
            if (!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(true);
            }
            String[] output = {"",""};

            Document doc;
            try {

                // need http protocol
                doc = Jsoup.connect(arg[0]).get();

                String temp = doc.html().replace("<br />", "==="); //$$$ instead <br>
                doc = Jsoup.parse(temp); //Parse again

                // get all links
                Elements paragraph = doc.select("div[class=poem__text] p");

                String result = paragraph.get(0).html();
                String[] separated = result.split("===");
                output[0] = "";

                for (int i = 0;i < separated.length;i++) {
                    String line = separated[i].trim();
                    line = line.substring(0,1).toUpperCase() + line.substring(1).toLowerCase();
                    output[0] = output[0] + line.trim() + "\n";
                }

                Elements jsonElement = doc.select("div.b-page__content div.poem");
                String json = jsonElement.attr("onclick").toString();
                int beginIndex = json.indexOf("\"id\":\"")+6;
                int endIndex = json.indexOf("\",\"next-id");
                output[1] = json.substring(beginIndex,endIndex);

            } catch (IOException e) {
                e.printStackTrace();
            }

            return output;
        }

        protected void onPostExecute(String[] output) {
            swipeRefreshLayout.setRefreshing(false);

            TextView textView = (TextView) findViewById(R.id.main_text);
            textView.setText(output[0]);

            String playStoreLink = "https://play.google.com/store/apps/details?id=" +
                    getPackageName();

            if (output[0].length() > 85) {
                output[0] = output[0].substring(0,85) + "...";
            }

            String yourShareText = output[0] + "\nПодсмотрено в Стихоплёте: http://stihoplet.space-voyage.ru/"+output[1];
            Intent shareIntent = ShareCompat.IntentBuilder.from(parentActivity)
                    .setType("text/plain").setText(yourShareText).getIntent();
            // Set the share Intent
            mActionProvider.setShareIntent(shareIntent);

            AnimationSet animation = new AnimationSet(false); //change to false
            animation.addAnimation(fadeIn);

            textView.startAnimation(animation);

            SwipeRefreshLayout relativeLayout = (SwipeRefreshLayout) findViewById(R.id.main_container);

            Random ran = new Random();
            Integer cnt = ran.nextInt(colors.length);
            relativeLayout.setBackgroundColor(Color.parseColor(colors[cnt]));
        }
    }
}
